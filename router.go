package main

import ("github.com/gorilla/mux"
	"net/http"
	"encoding/json")

var sc *StudentController = &StudentController{} 

func NewRouter() *mux.Router {
	
	r := mux.NewRouter()
	r.HandleFunc("/student", StudentHandler)
	
	return r
}

func StudentHandler(w http.ResponseWriter, r *http.Request) {
	
	if(r.Method == "POST") {
		sc.Insert(r.Body)
		w.WriteHeader(201)
		r, _ := json.Marshal(HTTPResponse{Code: 201, Message: "Student Created"})
		w.Write(r)
		return
	}
	
	if(r.Method == "GET") {
		student := sc.SearchByRA(r.URL.Query().Get("ra"))
		w.WriteHeader(200)
		r, _ := json.Marshal(student)
		w.Write(r)
		return
	}
	
}











