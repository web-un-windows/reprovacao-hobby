package main

type HTTPResponse struct {
	Code int
	Message string
}
