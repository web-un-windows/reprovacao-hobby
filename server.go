package main

import ("net/http"
	"log"
	"time")


func StartServer() {
	
	s := &http.Server{
		Addr:           "172.22.51.147:8080",
		Handler: 	NewRouter(),
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
	}
	
	log.Fatal(s.ListenAndServe())
}
