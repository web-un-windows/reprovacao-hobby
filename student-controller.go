package main

import ("encoding/json"
	"io")

type StudentController struct {
	
	Students []Student
}

func (sc *StudentController) Insert(body io.ReadCloser) error {
	
	var s Student
	
	err := json.NewDecoder(body).Decode(&s)
	
	if (err != nil) {
		return err
	}
	
	sc.Students = append(sc.Students, s)
	return nil
} 

func (sc *StudentController) SearchByRA(ra string) Student {
	for i := 0; i < len(sc.Students); i++ {
		if(ra == sc.Students[i].RA) {
			return sc.Students[i]
		}
	}
	
	return Student{}
} 













